import argparse

import requests

from settings import settings
from src.main import guild_members_update
from src.models import Field, Embed

parser = argparse.ArgumentParser(
                    prog='Discord Interactions',
                    description='Quick and helpfull commands')


parser.add_argument('guild_id')

args = parser.parse_args()


def update_all_members(guild_id):
    changelogs, errors = guild_members_update(guild_id)

    if not changelogs and not errors:
        raise Exception("No changelog nor errors?")
    print("**changelogs**")
    print(changelogs)

    print("**errors**")
    print(errors)

    embeds = []
    fields = []
    fields.append(
        Field(
            name="Users",
            value="\n".join(
                [f"<@{changelog.id}> {changelog.message()}" for changelog in changelogs]
            ),
        ),
    ) if changelogs else None
    fields.append(
        Field(
            name="Errors",
            value="\n".join([str(error) for error in errors]),
        )
    ) if errors else None
    success_embed = Embed(title="Change Log", color=7419530, fields=fields)
    embeds.append(success_embed)
    message = "All members updated"

    request = requests.post(
        settings.discord_log_webhook_url,
        json={
            "username": "#Logs",
            "content": message,
            "embeds": [embed.dict(exclude_unset=True) for embed in embeds],
        },
    )
    request.raise_for_status()

update_all_members(args.guild_id)
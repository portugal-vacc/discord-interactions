# syntax=docker/dockerfile:1
FROM python:3.10-slim

WORKDIR /code

RUN pip install --upgrade pip

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

COPY . .

RUN pip install -r requirements.txt

EXPOSE 8100

CMD ["gunicorn", "--bind", "0.0.0.0:8100", "app:app"]
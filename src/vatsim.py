from enum import Enum
import requests

from src.models import VatsimUser


class Ratings(Enum):
    INA = -1
    SUS = 0
    OBS = 1
    S1 = 2
    S2 = 3
    S3 = 4
    C1 = 5
    C2 = 6
    C3 = 7
    I1 = 8
    I2 = 9
    I3 = 10
    SUP = 11
    ADM = 12


class PilotRatings(Enum):
    P0 = 0
    PPL = 1
    IR = 3
    CMEL = 7
    ATPL = 15
    FI = 31
    FE = 63


def get_vatsim_user_data(vatsim_id: int) -> VatsimUser | None:
    response = requests.get(f"https://api.vatsim.net/api/ratings/{vatsim_id}/")
    return VatsimUser(**response.json())

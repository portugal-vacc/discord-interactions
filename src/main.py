import re
from src.models import ChangeLog, DiscordUser, GuildMember
from src.exceptions import (
    RoleNameNotOnGuild,
    UnknownVatsimRatings,
    UserHasElevatedRoles,
    VatsimDataParseError,
    VatsimIdParseError,
)
from src.discord import (
    apply_roles_to_user,
    clear_guild_member,
    get_guild_members,
    get_guild_roles,
)
from tqdm import tqdm

from src.vatsim import PilotRatings, Ratings, get_vatsim_user_data


def guild_members_update(guild_id: int) -> tuple[list[ChangeLog], list[str]]:
    results = []
    errors = []

    for member in tqdm(get_guild_members(guild_id)):
        try:
            result = update_roles(member, member.user, guild_id)
            results.append(result) if result else None
        except Exception as err:
            errors.append(err)

    return results, errors


def parse_vatsim_user_id(input: str) -> int | None:
    matches = re.findall(r"\d{6,7}", input)
    if not matches:
        return None
    return matches[-1]


def update_roles(
    guild_member: GuildMember, discord_user: DiscordUser, guild_id: int
) -> ChangeLog | None:
    # parse vatsim user id
    vatsim_user_id = parse_vatsim_user_id(guild_member.nick or discord_user.username)
    if not vatsim_user_id:
        raise VatsimIdParseError(
            f"Can't find/parse VATSIM ID of {guild_member.nick or discord_user.username}"
        )

    # vatsim user data
    vatsim_user_data = get_vatsim_user_data(vatsim_user_id)
    if not vatsim_user_data:
        raise VatsimDataParseError(
            f"Can't find/parse VATSIM User Data of {guild_member.nick or discord_user.username}"
        )

    # get guild roles
    guild_roles = get_guild_roles(guild_id)

    # check if ratings are valid
    if vatsim_user_data.rating <= 0 or vatsim_user_data.pilotrating <= -1:
        try:
            clear_guild_member(
                guild_id=guild_id,
                guild_member_id=discord_user.id,
                everyone_role_id=guild_roles.get("@everyone"),
            )
        except UserHasElevatedRoles:
            raise UserHasElevatedRoles(
                f"{guild_member.nick or discord_user.username} has elevated roles."
            )
        return ChangeLog(
            old_nick=guild_member.nick,
            id=discord_user.id,
            username=discord_user.username,
            roles_before=guild_member.roles,
            roles_now=guild_roles.get("@everyone"),
        )

    # match roles to vatsim ratings
    try:
        rating_name = Ratings(vatsim_user_data.rating).name
        pilotrating_name = PilotRatings(vatsim_user_data.pilotrating).name
    except ValueError:
        raise UnknownVatsimRatings(
            f"{guild_member.nick or discord_user.username} ratings not in known database"
        )

    # get guild roles based on name
    try:
        guild_rating_role = None
        if vatsim_user_data.rating >= 2:
            guild_rating_role = guild_roles[rating_name]
        guild_pilotrating_role = None
        if vatsim_user_data.pilotrating >= 1:
            guild_pilotrating_role = guild_roles[pilotrating_name]
        portuguese_member_role = guild_roles["Portugal vACC Member"]
        portuguese_controller_role = guild_roles["Portugal vACC Controllers"]
        external_member_role = guild_roles["External vACC Member"]
    except KeyError:
        raise RoleNameNotOnGuild(
            f"Ratings {pilotrating_name} or {rating_name} are not on the guild roles names"
        )

    # match roles to vatsim subdivision
    user_roles = []

    if vatsim_user_data.subdivision != "POR":
        if vatsim_user_data.rating >= 11:
            user_roles.append(rating_name)
        user_roles.append(external_member_role)
    else:
        user_roles.append(portuguese_member_role)
        user_roles.append(guild_pilotrating_role) if guild_pilotrating_role else None
        if vatsim_user_data.rating >= 2:
            user_roles.append(portuguese_controller_role)
            user_roles.append(guild_rating_role)

    # are there rules to apply?
    if sorted(int(role) for role in user_roles) == sorted(
        [int(role) for role in guild_member.roles]
    ):
        return None

    # apply roles to user

    try:
        apply_roles_to_user(
            guild_id=guild_id,
            guild_member_id=discord_user.id,
            roles=user_roles,
        )
    except UserHasElevatedRoles:
        raise UserHasElevatedRoles(
            f"{guild_member.nick or discord_user.username} has elevated roles."
        )

    return ChangeLog(
        nick=guild_member.nick,
        id=discord_user.id,
        username=discord_user.username,
        roles_before=guild_member.roles,
        roles_now=user_roles,
    )

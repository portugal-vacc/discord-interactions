class VatsimIdParseError(Exception):
    pass


class VatsimDataParseError(Exception):
    pass


class UnknownVatsimRatings(Exception):
    pass


class RoleNameNotOnGuild(Exception):
    pass


class UserHasElevatedRoles(Exception):
    pass

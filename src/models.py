from datetime import datetime
from typing import Optional
from pydantic import BaseModel


class DiscordUser(BaseModel):
    # avatar: Optional[str] = None
    # avatar_decoration_data
    # discriminator: str
    # global_name
    id: str
    # public_flags: int
    username: str


class GuildMember(BaseModel):
    # avatar: Optional[str] = None
    # communication_disabled_until: None
    # flags: int
    # joined_at: datetime
    nick: Optional[str] = None
    # pending: bool
    # permissions: str
    # premium_since: Optional[datetime]
    roles: list
    # unusual_dm_activity_until: None
    user: Optional[DiscordUser] = None
    # mute: bool
    # deaf: bool


class GuildRole(BaseModel):
    id: int
    name: str
    # color: int
    # icon: Optional[str]


class Guild(BaseModel):
    id: int
    # name: str
    # icon: Optional[str]
    # description: Optional[str]
    # owner_id: Optional[str]
    roles: list[Optional[GuildRole]]
    # system_channel_id: Optional[str]
    # rules_channel_id: Optional[str]


class ChangeLog(BaseModel):
    old_nick: Optional[str] = None
    nick: Optional[str] = None
    id: str
    username: str
    roles_before: list
    roles_now: list

    def message(self) -> str | None:
        message = []
        if len(self.roles_before) > len(self.roles_now):
            message.append("Roles Removed")

        if len(self.roles_before) < len(self.roles_now):
            message.append("Roles Added")

        if self.old_nick and not self.nick:
            message.append("Removed Nick")

        return " & ".join(message)


class VatsimUser(BaseModel):
    id: str
    rating: int
    pilotrating: int
    # militaryrating: int
    # susp_date: Optional[datetime]
    # reg_date: datetime
    # region: str
    # division: Optional[str]
    subdivision: Optional[str]
    # lastratingchange: Optional[datetime]


class Footer(BaseModel):
    text: str
    icon_url: Optional[str]


class Author(BaseModel):
    name: str
    url: Optional[str]
    icon_url: Optional[str]


class Field(BaseModel):
    name: str
    value: str
    inline: Optional[bool] = False


class Thumbnail(BaseModel):
    url: str


class Embed(BaseModel):
    title: Optional[str]
    type: str = "rich"
    description: Optional[str] = ""
    url: Optional[str] = "https://www.portugal-vacc.org/"
    timestamp: Optional[int] = int(datetime.utcnow().timestamp())
    color: Optional[int]
    footer: Optional[Footer] = dict()
    thumbnail: Optional[Thumbnail] = dict()
    author: Optional[Author] = dict()
    fields: Optional[list[Field]] = []

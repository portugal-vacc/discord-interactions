from typing import Optional
import requests
from src.models import Guild, GuildMember
from src.exceptions import UserHasElevatedRoles
from settings import settings
from functools import lru_cache


def generate_authorization_headers(application_token: str) -> dict:
    return dict(Authorization=f"Bot {application_token}")


@lru_cache()
def api_get(endpoint: str) -> dict:
    """Uses BOT authentication"""
    response = requests.get(
        f"{settings.discord_api_base}{endpoint}",
        headers=generate_authorization_headers(settings.discord_application_token),
    )
    response.raise_for_status()
    return response.json()


@lru_cache()
def get_guild(guild_id) -> Guild:
    """/guilds/{guild.id}"""
    payload = api_get(f"/guilds/{guild_id}")
    return Guild(**payload)


def apply_roles_to_user(
    guild_id: int,
    guild_member_id: int,
    roles: Optional[list[int]] = [],
) -> bool:
    """Returns a 200 OK with the guild member as the body."""
    response = requests.patch(
        f"{settings.discord_api_base}/guilds/{guild_id}/members/{guild_member_id}",
        json={"roles": roles},
        headers=generate_authorization_headers(settings.discord_application_token),
    )
    if response.status_code == 204:
        return True
    elif response.status_code == 403:
        raise UserHasElevatedRoles()
    response.raise_for_status()
    return False


@lru_cache()
def get_guild_roles(guild_id: int) -> dict[str, int]:
    """str, GuildRole | str -> role_name"""
    guild = get_guild(guild_id)
    return {role.name: role.id for role in guild.roles}


def clear_guild_member(
    guild_id: int, guild_member_id: int, everyone_role_id: int
) -> bool:
    response = requests.patch(
        f"{settings.discord_api_base}/guilds/{guild_id}/members/{guild_member_id}",
        json={"nick": "", "roles": [everyone_role_id]},
        headers=generate_authorization_headers(settings.discord_application_token),
    )
    if response.status_code == 204:
        return True
    elif response.status_code == 403:
        raise UserHasElevatedRoles()
    response.raise_for_status()
    return False


def get_guild_members(guild_id: int) -> list[GuildMember]:
    payload = api_get(f"/guilds/{guild_id}/members?limit=1000")
    return [GuildMember(**member) for member in payload]

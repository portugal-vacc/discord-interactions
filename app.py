import requests
from src.models import DiscordUser, Embed, GuildMember, Field
from src.main import guild_members_update, update_roles
from settings import settings
from flask import Flask, jsonify, request

from discord_interactions import (
    verify_key_decorator,
    InteractionType,
    InteractionResponseType,
)

app = Flask(__name__)


def parse_interaction_payload(payload: str) -> tuple[GuildMember, DiscordUser]:
    return (
        GuildMember(
            **list(dict(payload.get("data").get("resolved").get("members")).values())[0]
        ),
        DiscordUser(
            **list(dict(payload.get("data").get("resolved").get("users")).values())[0]
        ),
        payload.get("guild_id"),
    )


@app.route("/interactions", methods=["POST"])
@verify_key_decorator(settings.client_public_key)
def interactions():
    if request.json.get("type") == InteractionType.APPLICATION_COMMAND:
        if request.json.get("data").get("name") == "Update Member":
            guild_member, discord_user, guild_id = parse_interaction_payload(
                request.json
            )
            if not guild_member or not discord_user or not guild_id:
                raise Exception("Couldn't parse Interaction payload")
            update_roles(guild_member, discord_user, guild_id)

    return jsonify({"type": InteractionResponseType.PONG})


@app.route("/guild/<guild_id>/members/update/roles", methods=["GET", "POST"])
def members_update_roles(guild_id):
    changelogs, errors = guild_members_update(guild_id)

    if not changelogs and not errors:
        return jsonify({"type": 2})

    embeds = []

    fields = []
    fields.append(
        Field(
            name="Users",
            value="\n".join(
                [f"<@{changelog.id}> {changelog.message()}" for changelog in changelogs]
            ),
        ),
    ) if changelogs else None
    fields.append(
        Field(
            name="Errors",
            value="\n".join([str(error) for error in errors]),
        )
    ) if errors else None
    success_embed = Embed(title="Change Log", color=7419530, fields=fields)
    embeds.append(success_embed)
    message = "All members updated"

    request = requests.post(
        settings.discord_log_webhook_url,
        json={
            "username": "#Logs",
            "content": message,
            "embeds": [embed.dict(exclude_unset=True) for embed in embeds],
        },
    )
    request.raise_for_status()
    return jsonify({"type": 2})

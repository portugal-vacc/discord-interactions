# Discord Interactions

## Objective
Easily update discord guild member roles.

## How?

### Assumptions
- User with nick, signifies a past registration. Nickname follows three possible patterns:
    - {first name} {last name} - {vatsimid}
    - {first} - {vatsimid}
    - {vatsimid}
- (Allowed edge case: discord username in this format)

### User perspective
0. Looking at the discord server;
1. Right click user;
2. Select `Apps`;
3. Select `Update Member` that has the bot logo.

### Geek perspective
0. Parse VATSIM ID from guild nickname or discord username
1. Get user ratings
    1. If ratings invalid, guild nickname and roles are removed
2. Get guild roles
3. Correlate roles to user
4. Apply roles to user

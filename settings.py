from dotenv import load_dotenv
from pydantic_settings import BaseSettings


load_dotenv()


class Settings(BaseSettings):
    debug: bool = False
    server_ip: str = "0.0.0.0"
    server_port: str = "8100"
    discord_api_base: str = "https://discord.com/api/v10"
    discord_application_token: str
    client_public_key: str
    discord_log_webhook_url: str

    class Config:
        extra = "ignore"
        env_file = ".env"
        env_file_encoding = "utf-8"


settings = Settings()
